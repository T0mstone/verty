## Planned features

_(These are written as if they already exist,
so they can just be copied into the "Features" section when implemented.)_

<!-- maybe-TODO: ver_discrim? -->

Additional top-level macro arguments:
- `enum`: Generate an enum that contains each version as a variant (i.e. the sum of all versions).
	- The enum type be given the input name by default.
	- The enum type will impl `From<>` for each version type.
- `base`: Generate a base type that contains only what all versions have in common (i.e. the intersection of all versions).
	- The base type be called `<input>Base` by default, where `<input>` is the name of the input type.
	- The base type will impl `From<>` for each version type, and for the enum type if that option is given.

Additional feature for `rename`:
	- `<n>` can also be `base` or `enum` if the respective option is given,
		in which case the corresponding types will be renamed.

Methods to upgrade older versions to newer versions will be generated.
The behavior for this depends on the kind of type it is:
- `struct`:
	- The default behavior is as follows:
		* removed fields are dropped
		* unchanged fields are just kept (moved to the corresponding field in the higher version type)
			<!-- TODO: how would this interact with ver_attr, ver_where and VerType? -->
		* added fields must be provided as parameters to the upgrade method
	- handling of added fields can be made automatic
		by giving that field a helper attribute
		that determines the value given to it when upgrading
		(in which case it won't appear as a parameter):
		* `#[upgrade = <expr>]`: The plain value `<expr>` (it may not depend on other fields' values).
		* `#[upgrade_with(<func>)]`: The value of a call to the function expression `<func>`
			with a reference of the previous version type as the argument.
		* `#[upgrade_from({ <$($field:ident),*$(,)?> } => <func>)]`: Similar to `upgrade_with`,
			but the arguments will be the given fields, by value.
			All of them need to have been removed in that upgrade.
- `enum`:
	- The default behavior is as follows:
		* removed variants result in a handler parameter to the upgrade method
		* unchanged variants are just kept (moved to the corresponding variant in the higher version)
			<!-- TODO: how would this interact with ver_attr, ver_where and VerType? -->
		* (added variants don't need to be considered by nature of an enum)
	- handling of removed variants can be made automatic
		by giving that variant a helper attribute
		that determines its output when upgrading
		(in which case there won't be a corresponding handler parameter):
		* `#[upgrade_to(<expr>)]`: The expression `<expr>`,
			which can depend on all the fields of that variant.
- `union`s can't be automatically upgraded, since there's no way for the automatic generation to know
	which fields are safe to use in context.

Methods to downgrade newer versions to older versions will be generated analogously.

You can apply `#[versioned]` to an `impl` item, in which case the impl will be expanded
with its subject type as a versioned type according to the arguments to `versioned`
and the helper attrs placed on the impl.