use std::ops::{Bound, RangeBounds as _};

#[derive(Debug, Copy, Clone)]
pub struct Interval {
	pub start: usize,
	pub end: Bound<usize>,
}

impl Default for Interval {
	fn default() -> Self {
		Self {
			start: 0,
			end: Bound::Unbounded,
		}
	}
}

impl Interval {
	pub fn contains(&self, value: usize) -> bool {
		(Bound::Included(self.start), self.end).contains(&value)
	}

	/// Get the highest explicitly mentioned version.
	///
	/// Since start-unbounded ranges are converted into intervals starting at 0,
	/// this will return `0` for intervals derived from such ranges.
	pub fn highest_mentioned(self) -> usize {
		match self.end {
			Bound::Included(end) => end,
			Bound::Excluded(end) => end,
			Bound::Unbounded => self.start,
		}
	}
}
