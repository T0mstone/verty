assert_expansion_eq!(unversioned: {
	enum Foo {
		A,
		B(u8),
		C { x: u16 }
	}
} => {
	enum FooV0 {
		A,
		B(u8),
		C { x: u16 }
	}
});

assert_expansion_eq!(added_variants: {
	enum Foo {
		A,
		#[ver = 1..]
		B,
		#[ver = 2..=3]
		C,
		#[ver = ..2]
		D
	}
} => {
	enum FooV0 {
		A,
		D
	}

	enum FooV1 {
		A,
		B,
		D
	}

	enum FooV2 {
		A,
		B,
		C
	}

	enum FooV3 {
		A,
		B,
		C
	}
});

assert_expansion_eq!(added_fields: {
	enum Foo {
		A(#[ver = 1..] u8, #[ver = ..=2] u16),
		B { x: bool, #[ver = 2..3] y: bool },
	}
} => {
	enum FooV0 {
		A(u16),
		B { x: bool }
	}

	enum FooV1 {
		A(u8, u16),
		B { x: bool }
	}

	enum FooV2 {
		A(u8, u16),
		B { x: bool, y: bool }
	}

	enum FooV3 {
		A(u8),
		B { x: bool }
	}
});

assert_expansion_eq!(added_variants_and_fields: {
	enum Foo {
		#[ver = 1..=2]
		A { x: bool, #[ver = 2..] y: bool },
		#[ver = ..2]
		B { a: u8, #[ver = 2..] b: u8 }
	}
} => {
	enum FooV0 {
		B { a: u8 }
	}

	enum FooV1 {
		A { x: bool },
		B { a: u8 }
	}

	enum FooV2 {
		A { x: bool, y: bool }
	}
});
