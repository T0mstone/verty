#![doc = include_str!("../README.md")]

use syn::Error;
use util::error_sink::ErrorSink;

use self::parse::Input;
use self::parse::args::parse_macro_args;

mod util {
	pub mod interval;
	pub mod error_sink;
}

mod parse;
mod process;
mod output;

#[cfg(any(test, doctest))]
mod tests;

#[proc_macro_attribute]
pub fn versioned(
	args: proc_macro::TokenStream,
	input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
	let mut errs = ErrorSink::new();

	let args = parse_macro_args(args.into(), &mut errs);

	// TODO: When impl support comes, change this back to `parse::<Input>`
	errs.combine_into(syn::parse::<syn::DeriveInput>(input).map(Input::Type))
		.and_then(|inp| match inp {
			Input::Type(inp) => process::type_::versioned(args, inp),
			Input::Impl(_inp) => unreachable!(),
		})
		.map(|out| out.print())
		.unwrap_or_else(Error::into_compile_error)
		.into()
}
